import { useState, useRef } from 'react';

import { BeatLoader } from 'react-spinners';
import { EditIcon, CheckIcon } from '@chakra-ui/icons';
import {
  Box,
  Container,
  VStack,
  HStack,
  FormControl,
  FormLabel,
  FormErrorMessage,
  PinInput,
  PinInputField,
  Input,
  Button,
  ButtonGroup,
  Editable,
  EditablePreview,
  EditableInput,
  Text,
  AlertDialog,
  AlertDialogOverlay,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogBody,
  AlertDialogFooter,
  Skeleton,
} from '@chakra-ui/react';

const PaymentDetailsVerified = () => {
  const cancelRef = useRef<HTMLButtonElement>(null);

  return (
    <AlertDialog
      onClose={() => {
        return;
      }}
      leastDestructiveRef={cancelRef}
      isOpen={false}
      isCentered
    >
      <AlertDialogOverlay>
        <AlertDialogContent>
          <AlertDialogHeader>Payment Details Verified</AlertDialogHeader>

          <AlertDialogBody>Do you wish to continue?</AlertDialogBody>

          <AlertDialogFooter>
            <Button colorScheme="telegram">Continue</Button>
            <Button ref={cancelRef} colorScheme="red" ml="4">
              Cancel
            </Button>
          </AlertDialogFooter>
        </AlertDialogContent>
      </AlertDialogOverlay>
    </AlertDialog>
  );
};

const PaymentDetailsForm = () => {
  const [isVerifying, setIsVeryfying] = useState(false);

  return (
    <>
      <PaymentDetailsVerified />
      <Container
        maxW="container.lg"
        px="16"
        py="8"
        border="2px"
        borderRadius="md"
        borderColor="gray.200"
        boxShadow="lg"
        centerContent
      >
        <VStack spacing={4} w="lg">
          <Text fontWeight="semibold" fontSize="lg">
            Enter Payment Information
          </Text>
          <FormControl isInvalid>
            <FormLabel>Name on Card</FormLabel>
            <Input placeholder="Alia Atreides" />
            <FormErrorMessage>This card name is invalid!</FormErrorMessage>
          </FormControl>
          <FormControl>
            <FormLabel>Card Number</FormLabel>
            <Input placeholder="1234-5678-8765-4321" />
            <FormErrorMessage>This card number is invalid!</FormErrorMessage>
          </FormControl>
          <FormControl>
            <FormLabel>Expiry Date</FormLabel>
            <Box d="flex" w="32">
              <Input placeholder="MM"></Input>
              <Input placeholder="YY"></Input>
            </Box>
          </FormControl>
          <FormControl>
            <FormLabel>CVC</FormLabel>
            <PinInput>
              <PinInputField />
              <PinInputField />
              <PinInputField />
            </PinInput>
          </FormControl>
          <ButtonGroup w="100%" justifyContent="right">
            <Button
              colorScheme="telegram"
              onClick={() => setIsVeryfying(true)}
              isLoading={isVerifying}
              spinner={<BeatLoader color="white" />}
            >
              Verify
            </Button>
            <Button colorScheme="red">Cancel</Button>
          </ButtonGroup>
        </VStack>
      </Container>
    </>
  );
};

const EditableContent = () => (
  <HStack spacing="2">
    <EditablePreview />
    <EditableInput />
    <CheckIcon color="green.300" />
    <EditIcon />
  </HStack>
);

const PaymentOverview = () => (
  <Container
    maxW="container.lg"
    px="16"
    py="8"
    border="2px"
    borderRadius="md"
    borderColor="gray.200"
    boxShadow="lg"
    centerContent
  >
    <VStack w="lg" spacing="4">
      <Text fontSize="xl" fontWeight="semibold">
        Payment Confirmation
      </Text>
      <FormControl>
        <FormLabel fontWeight="semibold">Cardholder Name</FormLabel>
        <Editable defaultValue="Jason Mendoza">
          <EditableContent />
        </Editable>
      </FormControl>
      <FormControl>
        <FormLabel fontWeight="semibold">Card Number</FormLabel>
        <Editable defaultValue="XXXX-XXXX-XXXX-0143">
          <EditableContent />
        </Editable>
      </FormControl>
      <FormControl>
        <FormLabel fontWeight="semibold">Expiry Date (MM/YY)</FormLabel>
        <Editable defaultValue="02/25">
          <EditableContent />
        </Editable>
      </FormControl>
      <FormControl>
        <FormLabel fontWeight="semibold">CVC</FormLabel>
        <Editable defaultValue="***">
          <EditableContent />
        </Editable>
      </FormControl>
      <ButtonGroup w="100%" justifyContent="right">
        <Button colorScheme="telegram">Confirm Details</Button>
        <Button colorScheme="red">Cancel</Button>
      </ButtonGroup>
    </VStack>
  </Container>
);

const Signup = () => {
  return (
    <Box w="100%" p="16">
      <PaymentOverview />
    </Box>
  );
};

export default Signup;
