import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { extendTheme, ChakraProvider } from '@chakra-ui/react';

import Home from './pages/home';
import Signup from './pages/signup';

const theme = extendTheme({
  colors: {
    utsWhite: '#ffffff',
    utsBlack: '#323232',
    utsRichBlack: '#000000',
    utsBlue: '#0f4beb',
    utsRichBlue: '#0d41d1',
    utsRed: '#ff2305',
    utsDarkGrey: '#767676',
    utsLightGrey: '#ebebeb',
  },
  styles: {
    global: {
      body: {
        bg: 'utsWhite',
        color: 'utsBlack',
      },
    },
  },
});

const App = () => (
  <BrowserRouter>
    <ChakraProvider theme={theme}>
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/signup">
          <Signup />
        </Route>
      </Switch>
    </ChakraProvider>
  </BrowserRouter>
);

export default App;
